"""
color_poster.py - Make a poster containing colors and their names
Written in 2013 by Stefan Parviainen <pafcu@iki.fi>
To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide. This software is distributed without any warranty. 
You should have received a copy of the CC0 Public Domain Dedication along
with this software.
If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 
 
Needs rgb.txt, in the format colorname\tcolorhex (see e.g. http://xkcd.com/color/rgb.txt)
"""

import math
import matplotlib
matplotlib.use('pdf') # PDF backend allows use without a graphical interface running
import matplotlib.pyplot as plt
import random

# Functions for calculating color contrast to pick best text color
# Based on http://www.w3.org/TR/WCAG20/#contrast-ratiodef
def luminance(color):
	"""Calculate relative luminance of given color (RGB normalized to 1)"""
	corrected = [c/12.92 if c <= 0.03928 else ((c+0.055)/1.055)**2.4 for c in color]
	return corrected[0]*0.2126 + corrected[1]*0.7152 + corrected[2]*0.0722

def contrast(lighter, darker):
	"""Calculate contrast between two colors (RGB normalized to 1)"""
	return (luminance(lighter)+0.05)/(luminance(darker)+0.05)

# TODO: Add option to give list of colors to compare with (i.e. generalize this)
def pick_text_color(colorhex):
	"""Pick text color that maximized contrast with given background (hex rgb)"""
	rgb = matplotlib.colors.hex2color(colorhex)
	black_contrast = contrast(rgb, (0,0,0))
	white_contrast = contrast((1,1,1), rgb)
	max_contrast = max(white_contrast, black_contrast)

	if max_contrast < 4.5: # Minimum for W3C Level AA regular sized text
		print "WARNING: Bad contrast", colorhex, max_contrast

	if white_contrast > black_contrast:
		return 'white'
	else:
		return 'black'

def add_color_box(ncol, nrow, colorname, colorhex, font):
	"""Draw a box with given background color and color name"""
	# Wrap long names. TODO: Fancier wordwrap might be needed
	colorname = colorname.replace(' ', '\n')

	# Add a rectangle with the color
	plt.gca().add_patch(matplotlib.patches.Rectangle((ncol,nrow), 1.0, 1.0, color=colorhex))

	# Add color name using appropriate text color
	textcolor = pick_text_color(colorhex)
	plt.text(ncol+0.5, nrow+0.5, colorname, ha='center', va='center', color=textcolor, fontsize=10, fontname=font)

def get_cols_rows(total, aspect_ratio):
	"""Determine the number of columns and rows to get correct aspect ratio"""
	# ncolors = nrows*ncols and aspect_ratio=ncolumns/nrows, solve for ncols, nrows
	ncols = math.sqrt(total*aspect_ratio)
	nrows = int(math.ceil(total/ncols))
	ncols = int(math.ceil(ncols))
	return ncols, nrows

def main():
	"""Main function. Where it all starts."""
	width = 33.11 # inches
	height = 46.81 # inches

	# TODO: Make text configurable so that it matches if a different source is used
	srctext = '949 named colors from http://xkcd.com/color/rgb/\nhttps://gitlab.com/saparvia/color_poster'
	srcfont = 'Domestic Manners'
	namefont = 'Domestic Manners'

	colors = []
	# Read in color names and values
	# TODO: Take input filename from command line
	for line in open('rgb.txt'):
		# TODO: Add support for other input formats
		if not line.startswith('#'):
			colors.append(line.strip().split('\t'))

	random.shuffle(colors) # Might get something nice in the end

	# Get number of columns and rows to cover all colors and maintain aspect ratio
	ncols, nrows = get_cols_rows(len(colors), width/height)

	# Make the figure
	plt.figure(figsize=(width, height)).add_subplot(111)
	plt.subplots_adjust(left=0.0, right=1.0, bottom=0.0, top=1.0) # Remove margins

	# Draw color boxes
	# Partition colors into segments so all colors on one row go together
	for nrow, row in enumerate([colors[i*ncols:(i+1)*ncols] for i in range(nrows)]):
		for ncol, (colorname, colorhex) in enumerate(row):
			add_color_box(ncol, nrow, colorname, colorhex, namefont)

	# Ensure correct limits
	plt.xlim((0, ncols))
	plt.ylim((0, nrows))
	plt.gca().invert_yaxis() # Make drawing direction go downward so we get the empty space there

	# Hide axes
	plt.gca().set_frame_on(False)
	plt.gca().get_xaxis().set_visible(False)
	plt.gca().get_yaxis().set_visible(False)

	# Add some text about source data
	plt.text(ncols-0.5, nrows, srctext, va='bottom', ha='right', fontsize=8, fontname=srcfont)

	# Done. TODO: Get output file name from command line
	plt.savefig('colors.pdf')

if __name__ == '__main__':
	main()
